function invert(testObject)  {
    const invertedArry= [];
    const keysTstObj=Object.keys(testObject);
    const valuesTsObj=Object.values(testObject);
    
    for(let ele=0;ele<keysTstObj.length;ele+=1) {
        invertedArry[ele] = [valuesTsObj[ele],keysTstObj[ele]]
    } return invertedArry
}module.exports = invert;