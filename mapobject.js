function mapobject(testObject,cb) {
    for (let ele in testObject) {
        let maped = cb(testObject[ele],ele,testObject);
        testObject[ele] = maped;
    } return testObject
}
module.exports = mapobject;