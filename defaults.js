function defaults(testObject,defaultProps) {
    for (let ele in defaultProps) {
        if (testObject[ele] === undefined) {
            testObject[ele] = defaultProps[ele];
        }
    }return testObject
}
module.exports = defaults;